from datetime import datetime, timedetla
from functools import wraps


def never_expires(fn):
    memo = {}
    @wraps(fn)
    def wrapper(*args, **kwargs):
        key = args + tuple(sorted(kwargs.items()))

        if key in memo:
            return memo[key]
        rv = fn(*args, **kwargs)
        memo[key] = rv
        return rv

    return wrapper


def expires_after(delta):
    def inner(fn):
        memo = {}
        @wraps(fn)
        def wrapper(*args, **kwargs):
            now = datetime.now()
            key = args + tuple(sorted(kwargs.items()))

            if key in memo and now < memo[key][1] + delta:
                return memo[key][0]
            rv = fn(*args, **kwargs)
            memo[key] = (rv, datetime.now())
            return rv

        return wrapper
    return inner


def retain_if_it_takes(delta):
    def inner(fn):
        memo = {}
        @wraps(fn)
        def wrapper(*args, **kwargs):
            key = args + tuple(sorted(kwargs.items()))

            if key in memo:
                return memo[key]
            now = datetime.now()
            rv = fn(*args, **kwargs)
            if delta < datetime.now() - now:
                memo[key] = (rv, datetime.now())
            return rv

        return wrapper
    return inner
